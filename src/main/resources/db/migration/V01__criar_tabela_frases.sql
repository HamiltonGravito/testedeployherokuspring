CREATE SEQUENCE mensagem_seq START 1 INCREMENT 1;

CREATE TABLE mensagens (
    id BIGINT PRIMARY KEY DEFAULT nextval('mensagem_seq') NOT NULL,
    nome_autor VARCHAR(100),
    livro VARCHAR(50),
    mensagem TEXT NOT NULL,
    path_imagem VARCHAR(100)
);