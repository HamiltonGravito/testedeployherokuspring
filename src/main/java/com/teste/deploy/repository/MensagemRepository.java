package com.teste.deploy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teste.deploy.model.MensagemModel;

public interface MensagemRepository extends JpaRepository<MensagemModel, Long> {

}
