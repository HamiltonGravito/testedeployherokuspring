package com.teste.deploy.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class MensagemService {

	//Essa String recebe o caminho raiz de onde a imagem será salva
	@Value("${teste.deploy.diretorio.raiz.imagem}")
	private String raizDiretorioImagens;
	
	//Essa String recebe o nome do diretorio de onde a imagem será salva
	@Value("${teste.deploy.diretorio.raiz.imagem}")
	private String diretorioDeImagens;

	public void gerenciarImagem(String diretorio, MultipartFile imagem) {
		Path diretorioImagem = Paths.get(this.raizDiretorioImagens, diretorio);
		Path arquivoImagem = diretorioImagem.resolve(imagem.getOriginalFilename());
		
		try {
			Files.createDirectories(diretorioImagem);
			imagem.transferTo(arquivoImagem.toFile());
		} catch (IOException e) {
			throw new RuntimeException("Problemas na tentativa de salvar arquivo.", e);
		}

	}

	public void salvarImagem(MultipartFile imagem) {
		gerenciarImagem(this.diretorioDeImagens, imagem);
	}

}
