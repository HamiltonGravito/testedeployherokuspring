package com.teste.deploy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "mensagens")
public class MensagemModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Size(max = 100)
	private String nomeAutor;
	@Size(max = 50)
	private String livro;
	@NotNull
	private String mensagem;
	@Size(max = 50)
	private String pathImagem;
	@Transient
	private MultipartFile imagem;

	public MensagemModel() {

	}

	public MensagemModel(String nomeAutor, String livro, String mensagem, String pathImagem, MultipartFile imagem) {
		nomeAutor = this.nomeAutor;
		livro = this.livro;
		mensagem = this.mensagem;
		pathImagem = this.pathImagem;
		imagem = this.imagem;
	}

	public Long getId() {
		return id;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}

	public String getLivro() {
		return livro;
	}

	public void setLivro(String livro) {
		this.livro = livro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getPathImagem() {
		return pathImagem;
	}

	public void setPathImagem(String pathImagem) {
		this.pathImagem = pathImagem;
	}

	public MultipartFile getImagem() {
		return imagem;
	}

	public void setImagem(MultipartFile imagem) {
		this.imagem = imagem;
	}
	
	@Override
	public String toString() {
		StringBuilder saida = new StringBuilder();
		
		saida.append("Id: " + this.id + "\n");
		saida.append("Autor: " + this.nomeAutor + "\n");
		saida.append("Livro: " + this.livro + "\n");
		saida.append("Mensagem: " + this.mensagem + "\n");
		saida.append("PathImagem: " + this.pathImagem + "\n");
		return saida.toString();
	}
}
