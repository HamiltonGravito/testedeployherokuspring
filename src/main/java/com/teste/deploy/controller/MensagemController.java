package com.teste.deploy.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.deploy.model.MensagemModel;
import com.teste.deploy.repository.MensagemRepository;

//Permite requisições de outra raiz
@CrossOrigin
@RestController
@RequestMapping("/mensagens")
public class MensagemController {
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@PostMapping
	public ResponseEntity<MensagemModel> salvar(@Valid @RequestBody MensagemModel mensagem, HttpServletResponse response){
		System.out.println(mensagem.toString());
		MensagemModel mensagemCriada = mensagemRepository.save(mensagem);
		return ResponseEntity.status(HttpStatus.CREATED).body(mensagemCriada);
	}
	

}
